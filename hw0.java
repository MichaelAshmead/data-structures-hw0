/**

HW0.java
Starter file for CS 226, Fall 2014.

Code completed by:

*/

public final class HW0 {


    //This main method contains some simple tests to get you started.
    //You'll need to test your code much more than this does!
    public static void main(String[] args) {
    
    
        System.out.println("\nTesting the maxSum method...");
        //create a 4x4 two-dimensional array
        int[][] matrix = { {1, 2, 3,  4},
		           {0, 0, 0,  0},
		           {1, 2, 3, 10},
	                   {4, 3, 2,  1} };
		                   
       	String maxSumLocation = maxSum(matrix);
       	System.out.println("The max sum appears in " + maxSumLocation);
       	System.out.println();
    
        System.out.println("\nTesting the closestVals method...");
        int[] list = {19, 0, -5, 4, 7, 10};
        int[] closeValPosns = closestVals(list);
       	if (closeValPosns.length == 2) {
            System.out.println("The pair of integers with smallest difference appear "
                               + "at positions " + closeValPosns[0] + " and " 
                               + closeValPosns[1]);
       	} else {
	    System.out.println("Uh oh. The result array didn't have two items in it.");
	}
	System.out.println();

    }
    
    
     /**
     * Determine which row or column in a matrix m has the largest sum.
     * The parameter m is not altered as a result of this method call.
     * Assumes that m != null, m.length > 0, and m[0].length > 0, and that
     * m is a rectangular (not necessarily square) matrix, 
     *  
     * If a row contains the maximum sum, return a string starting with "R" and 
     * then the number of the row with no spaces, for example "R0" or "R12". If
     * a column contains the maximum sum, return a string starting with "C" and  
     * then the number of the column with no spaces, for example "C0" or "C12".  
     * If there is more than one row or column with the maximum sum, return 
     * rows over columns first, then smaller indices over larger indices.
     * Thus, if rows 3, 5, and 12, and columns 0 and 2 all contained the same
     * sum and that sum was maximal, then the method would return "R3".
     */
    public static String maxSum(int[][] m) {
    
    	//this is just a stub so that HW0.java compiles before you do the assignment
    	return "RO"; 
    
    }
    
    
    /** 
     * Find the positions of the two values in an int array a that are closest
     * to each other on a number line. 
     * The parameter a is not altered as a result of this method call.
     * It returns an int array of length 2, where the elements of the
     * result are the indices of ints in array a that have the smallest
     * distance (absolute value of difference) of any pair of ints in nums.
     * 
     * If there is more than one pair of ints that meet this criteria, the
     * method returns the indices of the pair that contains the minimum index.
     * 
     * If there is more than one pair of ints with the minimum index, this
     * method returns the indices of the pair with the smaller second index.
     * 
     * The first element of the result is the smaller of the two indices.
     * For example given the array {5, 3, 21, 10, 12, 7} the method
     * would return {0, 1}.
    */    
    public static int[] closestVals(int[] a) {
    
    	//this is just a stub so that HW0.java compiles before you do the assignment
    	int[] result = {-1, -1};
    	return result;    

    }

}